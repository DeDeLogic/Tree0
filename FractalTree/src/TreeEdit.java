import java.awt.*;       // Using AWT's Graphics and Color
import java.awt.event.*; // Using AWT event classes and listener interfaces
import javax.swing.*;    // Using Swing components and containers
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXFrame; 
import org.jdesktop.swingx.JXCollapsiblePane; 
import org.jdesktop.swingx.VerticalLayout; 

 

/** Custom Drawing Code Template */
// A Swing application extends javax.swing.JFrame
@SuppressWarnings("serial")
public class TreeEdit extends JFrame {
   // Define constants
   public static final int CANVAS_WIDTH  = 1000;
   public static final int CANVAS_HEIGHT = 680;
   private JTextField recursivCount;  // Use Swing's JTextField instead of AWT's TextField
   private JTextField stemLength;
   private JTextField stemWidth;
   private JTextField rightAngle;
   private JTextField leftAngle;
   private JTextField rightAlpha;
   private JTextField leftAlpha;
   private JButton btnCount;    // Using Swing's JButton instead of AWT's Button
   private int count = 0;
 
   // Declare an instance of the drawing canvas,
   // which is an inner class called DrawCanvas extending javax.swing.JPanel.
 
   private DrawCanvas treeCanvas;
   private JPanel btnPanel;
	double in_left_alpha;
	double in_right_alpha;
	double in_left_angle;
	double in_right_angle;
	int in_recursion_level;
	double in_start_branch_length;
	double in_start_branch_width;
	Tree t;
 
   // Constructor to set up the GUI components and event handlers
   public TreeEdit() 
   {
	   
	   in_left_alpha= 1.5F;
	   in_right_alpha= 1.5F;
	   in_left_angle= 45; //degrees
	   in_right_angle= -45; //degrees
		
	   in_start_branch_length=100F;
	   in_start_branch_width=18;
	   in_recursion_level=9;
	   
	   treeCanvas = new DrawCanvas();    // Construct the drawing canvas
	 treeCanvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
 
      // Set the Drawing JPanel as the JFrame's content-pane
      Container mcp = getContentPane();
      mcp.setLayout(new BorderLayout());   // The content-pane sets its layout

      JXCollapsiblePane cp = new JXCollapsiblePane();

      // JXCollapsiblePane can be used like any other container
      cp.setLayout(new BorderLayout());

      // the Controls panel with a textfield to filter the tree
   
      
      btnPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 4, 0));
      btnPanel.setBorder(new TitledBorder("Tree variables"));
      
      btnPanel.add(new JLabel("Recursion Level"));
     recursivCount = new JTextField("8", 2);
     recursivCount.setEditable(true);
     btnPanel.add(recursivCount);

     btnPanel.add(new JLabel("Stem length"));
     stemLength = new JTextField("100", 3);
     stemLength.setEditable(true);
     btnPanel.add( stemLength );
     
     btnPanel.add(new JLabel("Stem width"));
     stemWidth = new JTextField("18", 3);
     stemWidth.setEditable(true);
     btnPanel.add( stemWidth );
     
     btnPanel.add(new JLabel("Right angle"));
     rightAngle = new JTextField("45", 3);
     rightAngle.setEditable(true);
     btnPanel.add( rightAngle );
     
     btnPanel.add(new JLabel("Left angle"));
     leftAngle = new JTextField("45", 3);
     leftAngle.setEditable(true);
     btnPanel.add( leftAngle );
     
     btnPanel.add(new JLabel("Right alpha"));
     rightAlpha = new JTextField("1.5", 3);
     rightAlpha.setEditable(true);
     btnPanel.add( rightAlpha );
     
     btnPanel.add(new JLabel("Left alpha"));
     leftAlpha = new JTextField("1.5", 3);
     leftAlpha.setEditable(true);
     btnPanel.add( leftAlpha );
     
     
     btnCount = new JButton("Draw Tree");
     btnPanel.add(btnCount);
     cp.add("Center", btnPanel);

     mcp.add("North",cp);
      
      // Show/hide the "Controls"
    JButton toggle = new JButton(cp.getActionMap().get(JXCollapsiblePane.TOGGLE_ACTION));
    toggle.setText("Show/Hide Search Panel");

    mcp.add("South", toggle);

    mcp.add(treeCanvas, BorderLayout.CENTER); 
  
    // Allocate an anonymous instance of an anonymous inner class that
    //  implements ActionListener as ActionEvent listener
    btnCount.addActionListener(new ActionListener() {
       @Override
       public void actionPerformed(ActionEvent evt) {
          //count = count +1;
        //  recursivCount.setText(count + "");
    	 in_left_angle =  Double.parseDouble(leftAngle.getText());
    	 in_right_angle = -Double.parseDouble(rightAngle.getText());
    	 in_left_alpha =  Double.parseDouble(leftAlpha.getText());
    	 in_right_alpha  = Double.parseDouble(rightAlpha.getText());
      	 in_start_branch_length = Double.parseDouble(stemLength.getText());
      	 in_start_branch_width = Double.parseDouble(stemWidth.getText()); 
      	 in_recursion_level = Integer.parseInt(recursivCount.getText());
      	 treeCanvas.repaint();
           requestFocus();
      
       }
    });
      
    
     
      setDefaultCloseOperation(EXIT_ON_CLOSE);   // Handle the CLOSE button
      pack();              // Either pack() the components; or setSize()
      setTitle("Fractal Tree");  // "super" JFrame sets the title
      setVisible(true);    // "super" JFrame show
   }
   
   
  

   /**
    * Define inner class DrawCanvas, which is a JPanel used for custom drawing.
    */
   private class DrawCanvas extends JPanel 
   {
	  
	     
      // Override paintComponent to perform your own painting
	   @Override
      public void paintComponent(Graphics g) 
      {
         super.paintComponent(g);     // paint parent's background
         setBackground(Color.WHITE);  // set background color for this JPanel
 
         g.setColor(Color.GREEN);    // set the drawing color
       
         // Your custom painting codes. For example,
         // Drawing primitive shapes
         g.setColor(Color.GRAY);    // set the drawing color
         g.drawLine(0, 600, 1000, 600);
        
         // Printing texts
         
         g.setColor(Color.LIGHT_GRAY ) ;  // change the drawing color
         
         g.fillRect(0, 601,CANVAS_WIDTH, 601);
         g.setColor(Color.DARK_GRAY);
         g.setFont(new Font("Monospaced", Font.PLAIN, 12));
         g.drawString("Trees ...", 100, 620);
         Tree t = new Tree( in_left_alpha, in_right_alpha, in_left_angle, in_right_angle,  
    			 in_start_branch_length, in_start_branch_width, in_recursion_level);
    //     Tree t = new Tree( in_left_alpha, in_right_alpha, in_left_angle, in_right_angle,  
    //			 in_start_branch_length, in_start_branch_width, in_recursion_level);
    	  	t.g = g;
    	  	t.Draw();
      }
   }
 
   // The entry main method
   public static void main(String[] args) 
   {
      // Run the GUI codes on the Event-Dispatching thread for thread safety
      SwingUtilities.invokeLater(new Runnable() {
         @Override
         public void run() {
            new TreeEdit(); // Let the constructor do the job
         }
      });
   }
  
     
}