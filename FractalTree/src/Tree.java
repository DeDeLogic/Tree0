
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;



public class Tree 
{
	public  int BRANCH_LEVEL;
	
	double left_alpha;
	double right_alpha;
	double left_angle;
	double right_angle;
	int recursion_level;
	double start_branch_length;
	double start_branch_width;
	int x1;
	int y1;
	Graphics g;
	
	Branch stem;
	
	
	public Tree()
	{
		left_alpha= 1.5F;
		right_alpha= 1.5F;
		left_angle= 45; //degrees
		right_angle= -45; //degrees
		
		start_branch_length=100F;
		start_branch_width=18;
		recursion_level= 8;
		BRANCH_LEVEL =8;
		
		stem = new Branch(500,0,start_branch_length,start_branch_width,0F,recursion_level,this);
	}
		
	public Tree( double in_left_alpha,	double in_right_alpha, 
			double in_left_angle, double in_right_angle,  
			double in_start_branch_length,	double in_start_branch_width, int in_recursion_level)
	{
			left_alpha= in_left_alpha;
			right_alpha= in_right_alpha;
			left_angle= in_left_angle; //degrees
			right_angle= in_right_angle; //degrees
			
			start_branch_length= in_start_branch_length;
			start_branch_width=in_start_branch_width;
			recursion_level= BRANCH_LEVEL= in_recursion_level;
			77

	
	public class Branch
	{
		double length;
		double width;
		double angle;
		int x1;
		int y1;
		public int x2;
		public int y2;
		int level;
		Tree t;
		Branch left_stem;
		Branch right_stem;
			
		public Branch(int i, int j, double branch_length, double branch_width, double b_angle, int recursion_level,Tree tree)
		{
			// TODO Auto-generated constructor stub
			length = branch_length;
			width = branch_width;
			angle = b_angle;
			x1 = i;
			y1 = j; 
			level = recursion_level;
			t = tree;
			x2 = x1 + (int)(length * Math.sin(Math.toRadians(angle)));
			y2 = y1 + (int)(length * Math.cos(Math.toRadians(angle)));
		
			if (level > 1 )
			{
				left_stem = new Branch(x2,y2, LeftBranchLength(), LeftBranchWidth(), angle + left_angle,level-1,t);
		
				right_stem = new Branch(x2,y2,RightBranchLength(),  RightBranchWidth(),angle + right_angle,level-1,t);
			}
		
		}
		
		private double LeftBranchLength()
		{
			return  length * Math.pow(2, -2/(3*left_alpha));
		}
		
		private double RightBranchLength()
		{
			return length * Math.pow(2, -2/(3*right_alpha));
		}
		
		private double LeftBranchWidth()
		{
			return  width * Math.pow(2, -1/(left_alpha));
		}
		
		private double RightBranchWidth()
		{
			return width * Math.pow(2, -1/(right_alpha));
		}
		
		
		///Color customColor = new Color(10, 255 - (255 * level / BRANCH_LEVEL), 10);
		
		public void Draw()
		{
		//	t.g.drawLine (x1, y1, x1, y1+(int)length);  
		//	
			/* trunk different from leaves */
			/*if (level == BRANCH_LEVEL)
				g.setColor(Color.DARK_GRAY );
			else
				g.setColor(Color.RED );*/
			
			Graphics2D g2 = (Graphics2D)g;
			g2.setStroke(new BasicStroke((int)width));
			//Color customColor = new Color(10, 225 - (225 * level / BRANCH_LEVEL), 10);
			int green = 225 - ((int)(225 * level) / BRANCH_LEVEL);
			Color customColor = new Color(10, green, 10);
			g2.setColor(customColor);
			t.g.drawLine (x1, 600-y1, x2, 600-y2);  
			if (left_stem != null)
				left_stem.Draw();
			if (right_stem != null)
				right_stem.Draw();
		}
		
	}
	


}
  
